<?php defined( '_JEXEC' ) or die( 'Restricted access' );?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" 
   xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<jdoc:include type="head" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates<?php echo $this->template; ?> /css/default.css" type="text/css" />
</head>

// Add this in the page divs so Joomla will use it's own system for content. If content is there, delete it.
<jdoc:include type="component" />
<jdoc:include type="modules" name="right" style="xhtml"/>  //left can be used, etc.
<jdoc:include type="modules" name="footer" style="xhtml"/>  // name just needs to be whatever the modules name is for that area on the page.

//change ul for navigation to a div
<jdoc:include type="modules" name="nav-main" style="xhtml"/> 
